import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

main() => runApp(PlayerApp());

class PlayerApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Dubplay",
      theme: ThemeData(
        primaryColor: Color(0xFF212022),
        accentColor: Colors.white,
      ),
      home: Player(),
    );
  }

}

class Player extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _Player();
  }
}

class Song {
  String _name;
  String _artist;
  String _coverPath;
  String _filePath;
  
  Song(String nameWhitoutExtention, String artistName){
    _name = nameWhitoutExtention;
    _artist = artistName;
    _coverPath = "assets/images/"+_name+".png";
    _filePath = "assets/mp3/"+_name+".mp3";
  }
}

class _Player extends State<Player> {
  AudioPlayer _audioPlayer = AudioPlayer();
  double _sliderValue = 0;
  double _lengthSong = 2500;
  String _musicStart = "00:00:00";
  String _musicEnd = "00:00:00";
  int _songSelected = 0;
  Map _songName = new Map();
  Song _instanceSong;
  IconData _playPause = Icons.play_arrow;

  ///
  /// Convert seconds on a string
  /// format hh:mm:ss
  ///
  String secondToString(double doubleValue){
    int seconds; int hours; int minutes;
    String sHours; String sMinutes; String sSeconds;

    seconds = doubleValue.toInt();
    hours = (seconds / 3600).floor();
    seconds = seconds % 3600;
    minutes = (seconds / 60).floor();
    seconds = seconds % 60;

    sHours = hours < 10 ? "0"+hours.toString() : hours.toString();
    sMinutes = minutes < 10 ? "0"+minutes.toString() : minutes.toString();
    sSeconds = seconds < 10 ? "0"+seconds.toString() : seconds.toString();

    return sHours+":"+sMinutes+":"+sSeconds;
  }

  Map makeMap(){
    Map map = new Map();
    map[0] = {"name":"flow", 'artist': 'XaeboR'};
    map[1] = {'name': "kraken", 'artist': "N3ÜRØ"};
    return map;
  }

  void changeSong(operation){
    int index = _songSelected;
    if(operation == "--"){
      index = (index - 1) < 0 ? _songName.length - 1 : --index;
    } else if(operation == "++"){
      index = (index + 1) >= _songName.length ? 0 : ++index;
    }
    setState(() {
      _songSelected = index;
    });
  }

  void playLocal() async {
    print(_instanceSong._filePath);

    if(_playPause == Icons.play_arrow){
      _playPause = Icons.pause;
    } else {
      await _audioPlayer.play(_instanceSong._filePath, isLocal: true);
    }

    setState(() {
      if(_playPause == Icons.play_arrow){
        _playPause = Icons.pause;
      } else {
        _playPause = Icons.play_arrow;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _musicEnd = secondToString(_lengthSong);
    });

    _songName = makeMap();

    _instanceSong = new Song(_songName[_songSelected]["name"], _songName[_songSelected]["artist"]);

    return Scaffold(
      backgroundColor: Color(0xFF434243),
      drawer: new Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                  child: DrawerHeader(
                    child: Center(
                      child: Text(
                        "Menu",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                        color : Color(0xFF212022)
                    ),
                  ),
                  height: 100
              ),
              ListTile(
                selected: true,
                title: Row(
                  children: <Widget>[
                    Icon(Icons.play_arrow),
                    Container(child: Text("Lecture"), padding: EdgeInsets.only(left: 10),)
                  ],
                ),
                subtitle: Text("Lire les pistes de la playlist"),
                onTap: ()=>{},
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Icon(Icons.list),
                    Container(child: Text("Liste des pistes"), padding: EdgeInsets.only(left: 10),)
                  ],
                ),
                subtitle: Text("Gestion de la playlist"),
                onTap: ()=>{},
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Icon(Icons.help_outline),
                    Container(child: Text("On verra"), padding: EdgeInsets.only(left: 10),)
                  ],
                ),
                subtitle: Text("Autre menu"),
                onTap: ()=>{},
              )
            ],
          )
      ),
      appBar: AppBar(
        title: Text(
          "Flutter Player",

        ),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.only(
              top: 20,
              left: MediaQuery.of(context).size.width/8,
              right: MediaQuery.of(context).size.width/8,
              bottom: 30
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Card(
                color: Colors.black,
                elevation: 5.0,
                child: Container(
                  height: 250,
                  width: 250,
                  child: Image.asset(
                    _instanceSong._coverPath,
                    fit: BoxFit.fitWidth,
                  )
                ),
              ),
              Text(
                _instanceSong._name.toUpperCase(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                ),
              ),
              Text(
                _instanceSong._artist,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 25
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon( //todo alignement
                      Icons.fast_rewind,
                      color: Colors.white,
                      size: 50,
                    ),
                      onPressed: ()=>{
                        changeSong("--")
                      }
                  ),
                  IconButton(
                      icon: Icon( //todo alignement
                        _playPause,
                        color: Colors.white,
                        size: 50,
                      ),
                      onPressed: ()=>{
                        playLocal()
                      }
                  ),
                  IconButton(
                      icon: Icon( //todo alignement
                        Icons.fast_forward,
                        color: Colors.white,
                        size: 50,
                      ),
                      onPressed: ()=>{
                        changeSong("++")
                      }
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    _musicStart,
                   style: TextStyle(
                     color: Colors.white
                   ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    _musicEnd,
                    style: TextStyle(
                        color: Colors.white
                    ),
                  )
                ],
              ),
              Slider(
                  value: _sliderValue,
                  min: 0,
                  max: _lengthSong,
                  activeColor: Colors.redAccent,
                  inactiveColor: Colors.white70,
                  divisions: _lengthSong.toInt(),
                  onChangeEnd: (double value){
                    setState(() {
                      _sliderValue = value.round().toDouble();
                      _musicStart = secondToString(_sliderValue);
                    });
                  },
                  onChanged: (double newValue){
                    setState(() {
                      _sliderValue = newValue.round().toDouble();
                    });
                  }
              ),
            ],
          ),
        )
        ),
      );
  }

}